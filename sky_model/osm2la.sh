#!/bin/bash
## Quick and Dirty to convert OSM file to input for LEAP 
## Use as `osm2la.sh sky.osm`
## i.e. LeapAccelerateCLI -f OSKAR_sim.ms -d `osm2la.sh sky.osm`


if [ $# -eq 0 ]; then
    echo $0, ": Give OSKAR MODEL FILE for conversion"
    exit 1
fi

awk 'BEGIN{printf("[");first=0}
{if (substr($1,1,1)!="#" && NF>4)
	{if (first) {printf(",");}
	  first=1
	  printf("[%.9f,%.9f]",$1*3.14159265359/180,$2*3.14159265359/180)}
}
END{print "]"}' $1
