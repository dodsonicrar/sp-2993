docker run -v /scratch/mrioja:/data -w /data fdulwich/oskar-python3:2.8.3 oskar_sim_interferometer hello_world.ini
docker run -v /scratch/mrioja:/data -w /data fdulwich/oskar-python3:2.8.3 python3 sim_interferometer_command_line.py hello_world.ini
singularity run /home/nsteyn/sp-3445/OSKAR-2.8.3-Python3.sif python3 sim_interferometer_command_line.py demo.ini
