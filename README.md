# SP-2993

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dodsonicrar/sp-2993.git
git branch -M main
git push -uf origin main
```

## History and Context

This repository is provided to fulfill some of the steps for SKA SDP project: https://jira.skatelescope.org/browse/SP-2993

* Evaluate existing dataset specifications for Low and Mid and revise to ensure we can suitably stress an AA2-scale imaging pipeline
* Should be designed to pose non-trivial problems in calibration (direction-dependent), imaging and deconvolution.
* Non-goal: Realism.
* Provide solution for generating datasets that can be run by anyone on the DP testing platform.
* Demonstrate by generating "decimated" data sets for development.

It sits above OSKAR and provides a simple interface for the simulations. 
The docker.cmd file lists how to use it with docker (or singularity), which are to be honest the only well tested options.
The k.cmd file lists how to get it to work with SKA kubectrl interface. 

hello_world.ini and demo.ini are small scale minimal starting points for the command line options
